package com.example.android_part_d_task_1;

/*

1. Создать список товаров. Каждый товар содержит 3-5 характеристик.
Список содержит только название.
При нажатии на товар появляется всплывающий диалог с полным описанием товара (все характеристики).

*/

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_d_task_1.model.Products;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private void initRecyclerView(){
        recyclerView = findViewById(R.id.ap_products_list);
//      recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        final Products products = new Products();
        MyAdapter adapter = new MyAdapter(this, products.getProductsName());
        recyclerView.setAdapter(adapter);
        adapter.setListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onClick(String productName) {

                String productCharacteristics = products.getProductCharacteristics(productName);

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                dialogBuilder.setTitle(productName)
                        .setMessage(productCharacteristics)
                        .setCancelable(false)
                        .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });

                AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });
    }
}
