package com.example.android_part_d_task_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<String> productsName;
    private Context context;
    private OnItemClickListener listener;

    interface OnItemClickListener {
        void onClick(String productName);
    }

    public MyAdapter(Context context, List<String> productsName) {
        super();

        this.setProductsName(productsName);
        this.setContext(context);
    }

    //================================================================================//

    public List<String> getProductsName() {
        return productsName;
    }

    public void setProductsName(List<String> productsName) {
        this.productsName = productsName;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnItemClickListener getListener() {
        return listener;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    //================================================================================//

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.products, parent, false);

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View rowView = layoutInflater.inflate(R.layout.products, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(rowView);

        return myViewHolder;
    }

    @Override
    // выполняется для каждого item
    // и служит для передачи данных в layout
    // в данном случае для передачи данных из списка products
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        String productName = productsName.get(position);
        holder.productName.setText(productName);
    }

    @Override
    public int getItemCount() {
        return productsName.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView productName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            productName = itemView.findViewById(R.id.product_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(productsName.get(getAdapterPosition()));
                }
            });
        }
    }
}

