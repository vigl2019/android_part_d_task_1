package com.example.android_part_d_task_1.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Products {

    private Map<String, List<String>> products = new LinkedHashMap<>();

    /*
        {
            products.put("Product_1", new ArrayList<String>(3));
            products.put("Product_2", new ArrayList<String>(3));
            products.put("Product_3", new ArrayList<String>(3));
            products.put("Product_4", new ArrayList<String>(3));
            products.put("Product_5", new ArrayList<String>(3));

            products.get("Product_1").set(0, "Characteristic_1");
            products.get("Product_1").set(1, "Characteristic_2");
            products.get("Product_1").set(2, "Characteristic_3");

            products.get("Product_2").set(0, "Characteristic_1");
            products.get("Product_2").set(1, "Characteristic_2");
            products.get("Product_2").set(2, "Characteristic_3");

            products.get("Product_3").set(0, "Characteristic_1");
            products.get("Product_3").set(1, "Characteristic_2");
            products.get("Product_3").set(2, "Characteristic_3");

            products.get("Product_4").add(0, "Characteristic_1");
            products.get("Product_4").add(1, "Characteristic_2");
            products.get("Product_4").add(2, "Characteristic_3");

            products.get("Product_5").add(0, "Characteristic_1");
            products.get("Product_5").add(1, "Characteristic_2");
            products.get("Product_5").add(2, "Characteristic_3");
        }
    */
    public Products() {
        productsInit();
    }

    //================================================================================//

    public List<String> getProductsName() {

        List<String> productsName = new ArrayList<>();

        for (String key : this.getProducts().keySet()) {
            productsName.add(key);
        }

        return productsName;
    }

    public String getProductCharacteristics(String productName) {

        String productCharacteristics = "";
        List<String> productCharacteristicsList = this.getProducts().get(productName);

        for (int i = 0; i < productCharacteristicsList.size(); i++)
            productCharacteristics += productCharacteristicsList.get(i).trim() + "\n";

        return productCharacteristics;
    }

    public Map<String, List<String>> getProducts() {
        return products;
    }

    public void setProducts(Map<String, List<String>> products) {
        this.products = products;
    }

    public void productsInit() {
        products.put("Product_1", new ArrayList<String>(3));
        products.put("Product_2", new ArrayList<String>(3));
        products.put("Product_3", new ArrayList<String>(3));
        products.put("Product_4", new ArrayList<String>(3));
        products.put("Product_5", new ArrayList<String>(3));

        products.get("Product_1").add(0, "Characteristic_1");
        products.get("Product_1").add(1, "Characteristic_2");
        products.get("Product_1").add(2, "Characteristic_3");

        products.get("Product_2").add(0, "Characteristic_1");
        products.get("Product_2").add(1, "Characteristic_2");
        products.get("Product_2").add(2, "Characteristic_3");

        products.get("Product_3").add(0, "Characteristic_1");
        products.get("Product_3").add(1, "Characteristic_2");
        products.get("Product_3").add(2, "Characteristic_3");

        products.get("Product_4").add(0, "Characteristic_1");
        products.get("Product_4").add(1, "Characteristic_2");
        products.get("Product_4").add(2, "Characteristic_3");

        products.get("Product_5").add(0, "Characteristic_1");
        products.get("Product_5").add(1, "Characteristic_2");
        products.get("Product_5").add(2, "Characteristic_3");
    }
}
